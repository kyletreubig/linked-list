#ifndef __SOURCE_LINKEDLIST_H__
#define __SOURCE_LINKEDLIST_H__

#include "Node.h"

/**
 * Singly-linked list of integer values.
 *
 * This class is NOT thread safe.
 *
 * Values can only be pushed or popped from the front of this linked list.
 *
 * The linked list can be reversed, which will re-order all nodes within
 * the list. Any pointers to nodes in the list obtained prior to reversal
 * will remain valid.
 *
 * Example:
 * @code
 * LinkedList list;
 * list.pushFront(3);
 * list.pushFront(1);
 * list.pushFront(4);
 *
 * list.getHead()->getData(); // will return 4
 * list.getHead()->getNext()->getData(); // will return 1
 * list.print(); // will print "4 1 3"
 *
 * list.reverse();
 * list.print(); // will print "3 1 4"
 * list.popFront(); // will return 3
 * list.popFront(); // will return 1
 * list.popFront(); // will return 4
 * @endcode
 */
class LinkedList {
private:
    Node* head;

public:
    /**
     * Initializes an empty linked list.
     */
    LinkedList();

    /**
     * Deletes all nodes in this linked list.
     */
    ~LinkedList();

    /**
     * Returns the head of this linked list.
     *
     * @returns Pointer to head node (memory ownership is not transferred!), may be nullptr
     */
    Node* getHead() const;

    /**
     * Reverses the order of all nodes in this linked list.
     */
    void reverse();

    /**
     * Removes any duplicate values from this linked list.
     *
     * Any previously-obtained pointers to nodes that are removed will no longer be valid.
     */
    void removeDuplicates();

    /**
     * Prints the contents of this linked list to stdout.
     */
    void print() const;

    /**
     * Adds the given value to front of this linked list.
     *
     * @param data New head node value
     */
    void pushFront(int data);

    /**
     * Removes the head node in this linked list and returns its value.
     *
     * @returns Value of head node
     */
    int popFront();
};

#endif // __SOURCE_LINKEDLIST_H__

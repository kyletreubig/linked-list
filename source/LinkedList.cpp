
#include <iostream>
#include <set>
#include "LinkedList.h"

LinkedList::LinkedList()
    : head(nullptr)
{}

LinkedList::~LinkedList() {
    while (head) {
        popFront();
    }
}

Node* LinkedList::getHead() const {
    return this->head;
}

void LinkedList::removeDuplicates() {
    std::set<int> unique;
    Node* last = nullptr;
    auto current = head;
    while (current) {
        int value = current->getData();
        if (unique.count(value) != 0u) {
            // Skip over this one (don't keep it)
            if (last) {
                last->setNext(current->getNext());
            }
            auto to_delete = current;
            current = current->getNext();
            delete to_delete;
        } else {
            unique.insert(value);
            last = current;
            current = current->getNext();
        }
    }
}

void LinkedList::reverse() {
    Node* reversed = nullptr;
    while (head) {
        // Do a "pop", but don't delete the node
        auto prev = head;
        head = prev->getNext();

        // Do a "push" onto the reversed list
        prev->setNext(reversed);
        reversed = prev;
    }
    head = reversed;
}

void LinkedList::print() const {
    auto next = head;
    bool first = true;
    while (next) {
        if (not first) {
            std::cout << " ";
        }
        first = false;
        std::cout << next->getData();
        next = next->getNext();
    }
    std::cout << std::endl;
}

void LinkedList::pushFront(int data) {
    auto node = new Node(data);
    node->setNext(head);
    head = node;
}

int LinkedList::popFront() {
    int result = -1;
    if (head) {
        // Remove the current head and move to the next node
        auto prev = head;
        head = prev->getNext();

        result = prev->getData();
        delete prev;
    }
    return result;
}

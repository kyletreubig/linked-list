#ifndef __SOURCE_NODE_H__
#define __SOURCE_NODE_H__

/**
 * Linked list node.
 *
 * This class is NOT thread safe.
 *
 * Each node in the linked list contains a pointer to the next node
 * in the list. All nodes are dynamically allocated on the heap, and
 * ownership of the memory belongs to the containing linked list.
 */
class Node {
private:
    int data;
    Node* next;

public:
    /**
     * Initializes this node with the given value.
     *
     * @param data Node value
     */
    Node(int data);

    /**
     * Returns the value of this node.
     *
     * @returns Node value
     */
    int getData() const;

    /**
     * Replaces the value of this node.
     *
     * @param data New node value
     */
    void setData(int d);

    /**
     * Returns the pointer to the next node in the linked list.
     *
     * @returns Pointer to next node (memory ownership is not transferred!), may be nullptr
     */
    Node* getNext() const;

    /**
     * Replaces the pointer to the next node in the linked list.
     *
     * @param n Pointer to next node (memory ownership is not transferred!), may be nullptr
     */
    void setNext(Node* n);
};

#endif // __SOURCE_NODE_H__

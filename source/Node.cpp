#include "Node.h"

Node::Node(int data)
    : data(data), next(nullptr)
{}

int Node::getData() const {
    return this->data;
}

void Node::setData(int d) {
    this->data = d;
}

Node* Node::getNext() const {
    return this->next;
}

void Node::setNext(Node* n) {
    this->next = n;
}

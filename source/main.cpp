/**
 * Reverse a Linked List
 *
 * This program is intended to be a coding test/example for Two Six Labs interview.
 *
 * Write a program to create a linked list. The class should be able to:
 *     - Add elements to the end of the list
 *     - Print the list from end to end
 *     - Reverse the list
 *
 * @author  Two Six Labs
 * @version 1.0
 * @since   2020-10-06
 */


// Imports
#include <iostream> 
#include "Node.h"
#include "LinkedList.h"
  
/* Driver program to test above function*/
int main() 
{ 
    
    // creating linked list
    LinkedList ll; 

    // pushing objects onto the list
    ll.pushFront(1); 
    ll.pushFront(2); 
    ll.pushFront(3); 
    ll.pushFront(1); 
    ll.pushFront(2); 
    ll.pushFront(3); 
    ll.pushFront(4); 
    ll.pushFront(5); 
    ll.pushFront(5); 
  
    std::cout << "Printing Linked List:" << std::endl;
    ll.print();  // 5 4 3 2 1 3 2 1 
  
    std::cout << "Reversing List" << std::endl;
    ll.reverse(); 
  
    std::cout << "Reversed Linked List:" << std::endl; 
    ll.print();  // 1 2 3 1 2 3 4 5 

    std::cout << "Removing Duplicates" << std::endl;
    ll.removeDuplicates();

    std::cout << "Unique Linked List:" << std::endl; 
    ll.print();  // 1 2 3 4 5 

    return 0;
}

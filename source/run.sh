#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# Compile and run the main application
#
# Arguments:
# N/A
#
# Example Call:
#    bash run.sh
# -----------------------------------------------------------------------------

###
# Main Execution
###

echo "$(date +%c): compiling main.cpp"
g++ -std=c++17 -Wall -Wextra -Weffc++ -Wpedantic main.cpp -o main Node.cpp LinkedList.cpp

echo "$(date +%c): Running ./maine"
./main

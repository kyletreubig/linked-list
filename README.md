# Two Six Labs Coding Challenge

This is the repository for the Two Six Labs Coding Challenge for C++ Developmers.

## Table of Contents

* [Problem Description](#problem-description)
* [Goals](#goals)
* [Building and Running](#building-and-running)
* [Test](#test)
* [Dependencies](#dependencies)
* [Notes](#notes)
* [TODO](#todo)

## Problem Description

Reverse a Singly Linked List

Given a singly Linked List (with a pointer to head node), reverse the linked list object and print the output.

## Goals

### Functional

The following functional requirements should be met:

* Fill out Node.cpp and LinkedList.cpp in source
* source/main.cpp should compile (using run.sh) and print a reversed linked list
* Tests should pass when following the README.md in tests/

### Non-Functional

We are looking for functional code, but we are also looking to gage your coding style. Please think about

* Comments/Documentation
* Tests
* Efficiency

## Building and Running

```bash
localhost$ cd source
localhost$ bash run.sh
Tue Oct  6 17:12:32 2020: compiling main.cpp
Tue Oct  6 17:12:33 2020: Running ./maine
Printing Linked List:
5 4 3 2 1 3 2 1
Reversing List
Reversed Linked List:
1 2 3 1 2 3 4 5
```

## Testing

```bash
cd test/build/
cmake ../source
>> output
make
>> output
./unitTestApp
>>
[==========] Running 3 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 2 tests from LinkedList
[ RUN      ] LinkedList.getHead_should_return_nullptr_on_construct
[       OK ] LinkedList.getHead_should_return_nullptr_on_construct (0 ms)
[ RUN      ] LinkedList.reverse_should_reverse_the_list_order
[       OK ] LinkedList.reverse_should_reverse_the_list_order (0 ms)
[----------] 2 tests from LinkedList (0 ms total)

[----------] 1 test from Node
[ RUN      ] Node.getData_should_return_data
[       OK ] Node.getData_should_return_data (0 ms)
[----------] 1 test from Node (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 3 tests.
```

## Dependencies/System Requirements

There should be no external dependencies for the base problem.

Tests require CMake and Google Test (which is installed with CMake). Please follow instructions [here](https://cmake.org/install/) to install CMake if not already installed.

## Notes

* N/A

## TODO

* N/A

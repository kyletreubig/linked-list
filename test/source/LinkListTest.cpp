#include <iostream>
#include "../../source/LinkedList.h"
#include "gtest/gtest.h"

TEST(LinkedList, getHead_should_return_nullptr_on_construct) {
    LinkedList ll;
    EXPECT_EQ(ll.getHead(), nullptr);
}

TEST(LinkedList, getHead_should_return_head_node_after_push) {
    LinkedList ll;
    ll.pushFront(1);
    ASSERT_NE(ll.getHead(), nullptr);
    EXPECT_EQ(ll.getHead()->getData(), 1);

    ll.pushFront(2);
    EXPECT_EQ(ll.getHead()->getData(), 2);

    ll.pushFront(3);
    EXPECT_EQ(ll.getHead()->getData(), 3);
}

TEST(LinkedList, popFront_should_remove_head_node) {
    LinkedList ll;
    ll.pushFront(3);
    ll.pushFront(2);
    ll.pushFront(1);

    EXPECT_EQ(1, ll.getHead()->getData());
    EXPECT_EQ(1, ll.popFront());
    EXPECT_EQ(2, ll.getHead()->getData());
    EXPECT_EQ(2, ll.popFront());
    EXPECT_EQ(3, ll.getHead()->getData());
    EXPECT_EQ(3, ll.popFront());
    EXPECT_EQ(ll.getHead(), nullptr);
}

TEST(LinkedList, node_setData_replaces_value_in_place) {
    LinkedList ll;
    ll.pushFront(3);
    ll.getHead()->setData(7);
    EXPECT_EQ(7, ll.popFront());
}

TEST(LinkedList, reverse_should_do_nothing_on_empty_list) {
    LinkedList ll;
    EXPECT_EQ(ll.getHead(), nullptr);
    ll.reverse();
    EXPECT_EQ(ll.getHead(), nullptr);
}

TEST(LinkedList, reverse_should_do_nothing_on_single_entry_list) {
    LinkedList ll;
    ll.pushFront(42);
    EXPECT_EQ(ll.getHead()->getData(), 42);
    ll.reverse();
    EXPECT_EQ(ll.getHead()->getData(), 42);
}

TEST(LinkedList, reverse_should_reverse_the_list_order) {
    // Initialize a list of 4 -> 3 -> 2 -> 1
    LinkedList ll;
    ll.pushFront(1);
    ll.pushFront(2);
    ll.pushFront(3);
    ll.pushFront(4);

    ll.reverse();

    // The list should now be 1 -> 2 -> 3 -> 4
    int result;
    result = ll.popFront();
    EXPECT_EQ(1, result);
    result = ll.popFront();
    EXPECT_EQ(2, result);
    result = ll.popFront();
    EXPECT_EQ(3, result);
    result = ll.popFront();
    EXPECT_EQ(4, result);

    // The list should now be empty.
    EXPECT_EQ(ll.getHead(), nullptr);
}

TEST(LinkedList, removeDuplicates_should_eliminate_nodes_with_duplicate_values) {
    LinkedList ll;
    ll.removeDuplicates();

    ll.pushFront(2);
    ll.removeDuplicates();

    ll.pushFront(2);
    ll.pushFront(4);
    ll.pushFront(8);
    ll.pushFront(6);
    ll.pushFront(4);
    ll.pushFront(8);
    ll.pushFront(6);
    ll.pushFront(8);
    ll.removeDuplicates();

    EXPECT_EQ(8, ll.popFront());
    EXPECT_EQ(6, ll.popFront());
    EXPECT_EQ(4, ll.popFront());
    EXPECT_EQ(2, ll.popFront());
    EXPECT_EQ(ll.getHead(), nullptr);
}

struct StdOutCapture {
    std::stringstream buffer;

    StdOutCapture() : old(std::cout.rdbuf(buffer.rdbuf())) {}
    ~StdOutCapture() {
        std::cout.rdbuf(old);
    }

private:
    std::streambuf* old;
};

TEST(LinkedList, print_should_write_list_content_to_stdout) {
    LinkedList ll;
    ll.pushFront(3);
    ll.pushFront(1);
    ll.pushFront(4);

    StdOutCapture capture;
    ll.print();
    EXPECT_EQ("4 1 3\n", capture.buffer.str());
}

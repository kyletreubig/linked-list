cmake_minimum_required(VERSION 3.2.0)

project(TwoSixLabsInterviewTest)
set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_VERBOSE_MAKEFILE ON)

################################################################################
# Boiler plate googletest code to download source from github and build
################################################################################
# Download and unpack googletest at configure time
configure_file(CMakeLists.txt.in googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
                 ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
                 EXCLUDE_FROM_ALL)
################################################################################
# End googletest boiler plate
################################################################################



set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-g -O0 -Wall -fprofile-arcs -ftest-coverage")
set(CMAKE_CXX_OUTPUT_EXTENSION_REPLACE ON)

# Create OBJECT_DIR variable
set(OBJECT_DIR ${CMAKE_BINARY_DIR}/CMakeFiles/unitTestApp.dir)
message("-- Object files will be output to: ${OBJECT_DIR}")

SET(UNIT_TEST_APP_SOURCE
    main.cpp
    LinkListTest.cpp
    NodeTest.cpp
)

SET(LINKED_LIST_TEST_APP_SOURCE
    ../../source/LinkedList.cpp
    ../../source/Node.cpp
)

add_executable(unitTestApp
    ${UNIT_TEST_APP_SOURCE}
    ${LINKED_LIST_TEST_APP_SOURCE}
)

find_package(Threads REQUIRED)

target_link_libraries(unitTestApp
    gtest
    gmock
    Threads::Threads
)

################################################################################
# gcov + lcov
################################################################################

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    SET(GCOV_TOOL gcov)
elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    SET(GCOV_TOOL ${CMAKE_SOURCE_DIR}/llvm-gcov.sh)
else()
    message(WARNING "unrecognized compiler: ${CMAKE_CXX_COMPILER_ID}")
endif()

# Create the gcov target. Run coverage tests with 'make lcov'.
add_custom_target(lcov
    COMMAND echo "=================== LCOV ===================="
    COMMAND ${CMAKE_MAKE_PROGRAM} test
    COMMAND mkdir -p coverage
    COMMAND lcov -c -d .  --gcov-tool ${GCOV_TOOL} -o coverage/lcov_coverage_raw.info
    COMMAND lcov --remove coverage/lcov_coverage_raw.info '*/test/*' '/usr/*' '7*' -o coverage/lcov_coverage.info
    COMMAND genhtml coverage/lcov_coverage.info --output-directory coverage/lcov_output > genhtml_stdout.tmp
    COMMAND cat genhtml_stdout.tmp | grep lines | grep -o '[0-9]*.[0-9]\%' | grep -o '[0-9]*.[0-9]' > coverage/line_coverage_percent.info
    COMMAND echo -n "LINE COVERAGE " && cat coverage/line_coverage_percent.info
    COMMAND rm -f genhtml_stdout.tmp
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)
add_dependencies(lcov unitTestApp)

# Make sure to clean up the coverage folder
set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES coverage)

# Create the gcov-clean target. This cleans the build as well as generated .gcda and .gcno files.
add_custom_target(scrub
    COMMAND ${CMAKE_MAKE_PROGRAM} clean
    COMMAND rm -f ${OBJECT_DIR}/*.gcno
    COMMAND rm -f ${OBJECT_DIR}/*.gcda
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

# Testing
enable_testing()

add_test(output_test ${CMAKE_CURRENT_BINARY_DIR}/unitTestApp)
set_tests_properties(output_test PROPERTIES FAIL_REGULAR_EXPRESSION "FAILED TEST")

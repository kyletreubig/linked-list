#include "../../source/Node.h"
#include "gtest/gtest.h"

TEST(Node, getData_should_return_data) {
    Node* node = new Node(1); 
    EXPECT_EQ(node->getData(), 1);
}

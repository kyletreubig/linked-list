#include <cstdint>
#include "gmock/gmock.h"

std::int32_t main(std::int32_t argc, char **argv) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
